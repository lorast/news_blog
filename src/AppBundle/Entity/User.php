<?php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * User
 */
class User extends BaseUser
{
    /**
     * @var int
     */
    protected $id;
    /**
     * @var string
     * @ORM\Column(name="surname", type="string", length=50)
     */
    protected $surname;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSurName()/*: string*/
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     */
    public function setSurName(string $surname)
    {
        $this->surname = $surname;
    }
}
