<?php

namespace Blog\BlogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'text',
                array(
                'label'=> 'Title',
                'attr'=>
                    array(
                        'class' => 'form-control',
                    )
                )
            )
            ->add('content', 'textarea',
                array(
                    'label'=> 'Content',
                    'attr'=>
                        array(
                            'class' => 'form-control',
                            )
                )
            )
            ->add('image', 'file',
                array(
                    'data_class'=> null,
                    'required' => false,
                    'attr'=>
                        array(
                            'class' => 'btn btn-info',
                            'maxSize' => '20M',
                            'mimeTypes' => 'image/*'
                            )
                )
            )
            ->add('published');
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Blog\BlogBundle\Entity\News',
            'validation_groups' => false,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'blog_blogbundle_news';
    }
}
