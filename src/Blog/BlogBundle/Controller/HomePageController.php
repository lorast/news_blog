<?php

namespace Blog\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

class HomePageController extends Controller
{

    public function indexAction()
    {
        $news = $em->getRepository('BlogBlogBundle:News')
            ->findAll();
        return $this->render('BlogBlogBundle:Home:index.html.twig', array('news' => $news));
    }
}