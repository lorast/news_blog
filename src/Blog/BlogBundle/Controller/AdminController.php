<?php

namespace Blog\BlogBundle\Controller;

use Blog\BlogBundle\Entity\News;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\Common\Collections\Criteria;
use Knp\Bundle\PaginatorBundle\KnpPaginatorBundle;

class AdminController extends Controller
{
    public function adminAction()
    {
        $auth_checker = $this->get('security.authorization_checker');
        $token = $this->get('security.token_storage')->getToken();
        $user = $token->getUser();
        $isRoleAdmin = $auth_checker->isGranted('ROLE_ADMIN');

        if (!$isRoleAdmin) {
            $this->redirectToRoute('fos_user_security_login');
        }

        $criteria = Criteria::create();
        $criteria
            ->orderBy(array('id' =>Criteria::DESC));

        $entityRepository = $this->getDoctrine()->getRepository('BlogBlogBundle:News');

        $paginator = $this->get('knp_paginator');

        $requests = $paginator->paginate(
            $entityRepository->matching($criteria),
            $this->get('request')->query->get('page', 1),
            10
        );
        return $this->render('BlogBlogBundle:Admin:admin.html.twig', array(
            'news' => $requests,
        ));
    }
}
