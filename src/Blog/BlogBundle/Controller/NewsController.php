<?php

namespace Blog\BlogBundle\Controller;

use Blog\BlogBundle\Entity\News;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\Common\Collections\Criteria;
use Knp\Bundle\PaginatorBundle\KnpPaginatorBundle;
use Symfony\Component\Security\Acl\Exception\Exception;
use Symfony\Component\Validator\Constraints\File as FileConstraits;

/**
 * News controller.
 * Class NewsController
 * @package Blog\BlogBundle\Controller
 */
class NewsController extends Controller
{
    /**
     * Lists all news entities.
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $criteria = Criteria::create();
        $criteria
            ->where($criteria->expr()->neq('published', 0))
            ->orderBy(array('id' =>Criteria::DESC));

        $entityRepository = $this->getDoctrine()->getRepository('BlogBlogBundle:News');

        $paginator = $this->get('knp_paginator');

        $requests = $paginator->paginate(
            $entityRepository->matching($criteria),
            $this->get('request')->query->get('page', 1),
            10
        );

        $view = 'BlogBlogBundle:Home:index.html.twig';

        return $this->render($view, array(
            'news' => $requests,
        ));
    }

    /**
     * Creates a new news entity.
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function newAction(Request $request)
    {
        $news = new News();
        $form = $this->createForm('Blog\BlogBundle\Form\NewsType', $news);

        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();

        $this->formFileCheck($request->files->get('image'));

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $news->getImage();
            if ($file == null) {
                $fileName = 'default.jpg';
            } else {
                $fileName = $this->saveImage($file);
            }

            $news->setImage($fileName);

            $em->persist($news);
            $em->flush();


            $request->getSession()
                ->getFlashBag()
                ->add('success', 'News added!')
            ;
            return $this->redirectToRoute('admin');
        }

        return $this->render('news/new.html.twig', array(
            'news' => $news,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a news entity.
     *
     * @param News $news
     * @return Response
     */
    public function showAction(News $news)
    {
        $deleteForm = $this->createDeleteForm($news);

        return $this->render('news/show.html.twig', array(
            'news' => $news,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing news entity.
     *
     * @param Request $request
     * @param News $news
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function editAction(Request $request, News $news)
    {
        $oldFile = $news->getImage();
        $editForm = $this->createForm('Blog\BlogBundle\Form\NewsType', $news);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $file = $news->getImage();
            //$fileName = $news->getImage();
            if ($file == null) {
                $news->setImage($oldFile);
                //$fileName = 'default.jpg';
            } else {
                $fileName = $this->saveImage($file);
                $news->setImage($fileName);
            }

            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('admin');
        }

        $request->getSession()
            ->getFlashBag()
            ->add('success', 'News edited!')
        ;
        return $this->render('news/edit.html.twig', array(
            'news' => $news,
            'edit_form' => $editForm->createView(),
//            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes news entity
     *
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function deleteAction(Request $request, $id)
    {

        $em = $this->getDoctrine()->getManager();
        $news = $em->getRepository('BlogBlogBundle:News')->find($id);
        if (!$news) {
            throw $this->createNotFoundException(
                'No news found for id ' . $id
            );
        }

        $form = $this->createFormBuilder($news)
            ->add('delete', 'submit')
            ->getForm();

        $form->handleRequest($request);
        if ($form->isValid() && $form->isSubmitted()) {
            $fileName = $news->getImage();
            $imagesDir = $this->container->getParameter('kernel.root_dir').'/../web/images/';

            if (file_exists($fileName) && $fileName != 'default.jpg') {
                unlink("$imagesDir . $fileName");
            }
            $em->remove($news);
            $em->flush();

            $request->getSession()
                ->getFlashBag()
                ->add('success', 'News deleted!')
            ;
            return $this->redirectToRoute('admin');
        }

        $build['form'] = $form->createView();
        return $this->render('news/delete.html.twig', $build);
    }

    /**
     * Mark entity as published
     *
     * @param Request $request
     * @param News $news
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function publishAction(Request $request, News $news)
    {
        $news->setPublished(true);

        $this->getDoctrine()->getManager()->flush();

        $request->getSession()
            ->getFlashBag()
            ->add('success', 'News published!')
        ;
        return $this->redirectToRoute('admin');
    }

    /**
     * Save logo jpeg file
     *
     * @param \Symfony\Component\HttpFoundation\File\File $file
     * @return string
     */
    private function saveImage($file)
    {
        $fileName = md5(uniqid()).'.'.$file->guessExtension();
        $file->move(
            $this->getParameter('images_directory'),
            $fileName
        );
        return $fileName;
    }

    /**
     * Check uploaded file
     * with size and type criteria
     *
     * @param $file
     */
    private function formFileCheck($file)
    {
        $fileConstraints = new FileConstraits(array
        (
            'maxSize' => '2M',
            'mimeTypes' => 'image/*'
        ));
        $fileConstraints->mimeTypes = 'msg.validator.file.import.mimeType';
        $fileConstraints->maxSizeMessage = 'msg.validator.file.import.maxFileSize';

        $errorList = $this->get('validator')->validate($file, $fileConstraints);
        if (count($errorList)) {
            $errorMessage = $errorList[0]->getMessage();
        }
    }

    /**
     * Creates a form to delete a news entity.
     *
     * @param News $news The news entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(News $news)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('news_delete', array('id' => $news->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
