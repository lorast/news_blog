### Application for show and manage news. ###

Roles: *User, Admin.*
Anonymous User can read news and register.
Registered User can log in and read news.
Admin can read news, add news, delete, edit and publish them.

Installation:
=================================================================

    1. Clone 'git clone https://lorast@bitbucket.org/lorast/news_blog.git'
       or download repository archive
    2. Run 'composer update' inside project directory, for install dependencies
       It can ask some questions:
        database_host
        database_port
        database_name
        database_user
        database_password
       Then, input your data.
    3. Set permissions, if it needed
    4. Create database, if it needed
    5. Then, run 'php app/console doctrine:migrations:diff', and 'php app/console doctrine:migrations:migrate' for migrate database schema


**Create admin user**: 'php app/console fos:user:create admin --super-admin'

**Admin page**: click “Manage”(/admin) link in header, when you logged in as admin